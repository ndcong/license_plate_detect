FROM python:3.7.16

WORKDIR /

RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  -y
RUN pip install opencv-python==4.6.0.66
RUN pip install Pillow==9.2.0
RUN pip install paddleocr==2.5.0.3
RUN pip install paddlepaddle==2.3.1
RUN pip install python-multipart==0.0.5
RUN pip install fastapi
RUN pip install "uvicorn[standard]"

COPY . .
CMD ["uvicorn", "main:app" , "--host=0.0.0.0", "--port=80"]

# tensorflow==2.12.0
# librosa==0.10.0.post2
# fastapi==0.95.1
# birdnetlib==0.4.1